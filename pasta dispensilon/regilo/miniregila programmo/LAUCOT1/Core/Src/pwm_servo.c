/*
 * pwm_servo.c
 *
 *  Created on: Oct 26, 2020
 *      Author: Sergey Peganov
 */

/* Includes ------------------------------------------------------------------*/
#include <stdlib.h>
#include <stdbool.h>

#include "pwm_servo.h"

#include "main.h"
#include "tim.h"

/* Constants -----------------------------------------------------------------*/
const unsigned int SERVO_FREC_Hz = 50;
unsigned int SERVO_PRE_div;
unsigned int SERVO_Period;
double PULSE_STEP;


struct TservoInit{
  TIM_HandleTypeDef* htim;
  uint32_t channel;
  uint32_t max_pulse;
  uint32_t min_pulse;
  double max_angle;
  double min_angle;
};

struct Tmove{
  double target_angle;
  volatile bool is_reached;
};

struct Tpwm{
  volatile int32_t vCCR;
  int32_t vCCR_step;
  int32_t target_vCCR;
  volatile uint32_t* pulse;
  uint32_t pulse_0;
  uint32_t vCCR_per_deg;
  uint32_t Ftim;
  uint32_t time_slice;
};

struct Tservo{
  struct TservoInit init;
  struct Tmove move;
  struct Tpwm  pwm;
}servo1, servo2;

/*
 * Расчёт полей, значения которых определяются из уже введённых
 * или не зависят от физической реализации
 */
void TservoCalc(struct Tservo* servo){
  servo->move.is_reached = 1;
  servo->move.target_angle = 0;

  servo->pwm.time_slice = 1;

  servo->pwm.vCCR_per_deg = (int32_t)(
	(servo->init.max_pulse - servo->init.min_pulse) /
	(servo->init.max_angle - servo->init.min_angle) *
	servo->pwm.Ftim * 65536 / 1000000
      );
  servo->pwm.pulse_0 =
      (uint32_t)(1.0 * servo->pwm.Ftim * servo->init.min_pulse * 1.0e-6) +
      (((int32_t)(servo->pwm.vCCR_per_deg * servo->init.min_angle)) >> 16) ;


  servo->pwm.target_vCCR = (int32_t) (
      servo->pwm.vCCR_per_deg * servo->move.target_angle);

  servo->pwm.vCCR_step = 0;
  servo->pwm.vCCR = servo->pwm.target_vCCR;
}

void Servo_init(){
  SERVO_PRE_div = HAL_RCC_GetPCLK1Freq() / (1UL << 16) / SERVO_FREC_Hz + 1;
  SERVO_Period  = HAL_RCC_GetPCLK1Freq() / (SERVO_FREC_Hz * SERVO_PRE_div);
  PULSE_STEP  = ((double)SERVO_PRE_div) / HAL_RCC_GetPCLK1Freq();

  servo1.init.htim = &htim15;
  servo1.init.channel = TIM_CHANNEL_1;

  servo1.init.min_angle = 0;
  servo1.init.max_angle = 187;
  servo1.init.min_pulse = 520;
  servo1.init.max_pulse = 2500;

  servo1.pwm.pulse = &(htim15.Instance->CCR1);
  servo1.pwm.Ftim = HAL_RCC_GetPCLK1Freq() / SERVO_PRE_div ;

  TservoCalc(&servo1);


  servo2.init.htim = &htim15;
  servo2.init.channel = TIM_CHANNEL_2;

  servo2.init.min_angle = 0;
  servo2.init.max_angle = 180;
  servo2.init.min_pulse = 544;
  servo2.init.max_pulse = 2400;

  servo2.pwm.pulse = &(htim15.Instance->CCR2);
  servo2.pwm.Ftim = HAL_RCC_GetPCLK1Freq() / SERVO_PRE_div ;

  TservoCalc(&servo2);
}


void ServoStart(double angle1, double angle2){
  htim15.Init.Prescaler = SERVO_PRE_div - 1;
  htim15.Init.Period = SERVO_Period - 1;

  angle1 = angle1 > servo1.init.min_angle ? angle1 : servo1.init.min_angle;
  angle1 = angle1 < servo1.init.max_angle ? angle1 : servo1.init.max_angle;

  servo1.move.target_angle = angle1;

  servo1.pwm.vCCR = servo1.pwm.vCCR_per_deg * servo1.move.target_angle;
  *(servo1.pwm.pulse) = servo1.pwm.pulse_0 + (servo1.pwm.vCCR >> 16);


  angle2 = angle2 > servo2.init.min_angle ? angle2 : servo2.init.min_angle;
  angle2 = angle2 < servo2.init.max_angle ? angle2 : servo2.init.max_angle;

  servo2.move.target_angle = angle2;

  servo2.pwm.vCCR = servo2.pwm.vCCR_per_deg * servo2.move.target_angle;
  *(servo2.pwm.pulse) = servo2.pwm.pulse_0 + (servo2.pwm.vCCR >> 16);


  HAL_TIM_PWM_Init(servo1.init.htim);
  HAL_TIM_PWM_Init(servo2.init.htim);

  HAL_TIM_PWM_Start(servo1.init.htim, servo1.init.channel);
  HAL_TIM_PWM_Start_IT(servo2.init.htim, servo2.init.channel);
}


void ServoMoveTo(double angle, uint32_t time_ms, struct Tservo* servo){

  angle = angle > servo->init.min_angle ? angle : servo->init.min_angle;
  angle = angle < servo->init.max_angle ? angle : servo->init.max_angle;

  if (abs(servo->move.target_angle - angle) * servo->pwm.vCCR_per_deg < 65536){
      servo->pwm.vCCR_step = 0;
      servo->move.is_reached = 1;
      return;
  }

  servo->move.target_angle = angle;
  servo->pwm.target_vCCR = servo->pwm.vCCR_per_deg * servo->move.target_angle;

  int32_t curr_vCCR = servo->pwm.vCCR;
  int32_t N_periods = time_ms * SERVO_FREC_Hz / 1000U ;
  if (N_periods <= 1){
      servo->pwm.vCCR_step = 0;
      servo->pwm.vCCR = servo->pwm.target_vCCR;
      *(servo->pwm.pulse) = servo->pwm.pulse_0 + (servo->pwm.vCCR >> 16);
      servo->move.is_reached = 1;
      return;
  }

  servo->pwm.vCCR_step = (servo->pwm.target_vCCR - curr_vCCR) *
      (int32_t)servo->pwm.time_slice / N_periods;

  servo->move.is_reached = 0;
}


void Servo1GoTo(double angle){
  ServoMoveTo(angle, 0, &servo1);
}


void Servo2GoTo(double angle){
  ServoMoveTo(angle, 0, &servo2);
}


void Servo1MoveTo(double angle, uint32_t time_ms){
  ServoMoveTo(angle, time_ms, &servo1);
}


void Servo2MoveTo(double angle, uint32_t time_ms){
  ServoMoveTo(angle, time_ms, &servo2);
}

void ServoIRQ(){
  if(!servo1.move.is_reached){
      // Проверяем окончание движения в зависимости от направления
      if(servo1.pwm.vCCR_step > 0){
	  if(servo1.pwm.vCCR > servo1.pwm.target_vCCR){
	      servo1.move.is_reached = 1;
	      goto end_servo1;
	  }
      }else if(servo1.pwm.vCCR_step < 0){
	  if(servo1.pwm.vCCR < servo1.pwm.target_vCCR){
	      servo1.move.is_reached = 1;
	      goto end_servo1;
	  }
      }

      *(servo1.pwm.pulse) = (int32_t)servo1.pwm.pulse_0 + (servo1.pwm.vCCR >> 16);
      servo1.pwm.vCCR += servo1.pwm.vCCR_step;
end_servo1:{}
  }


  if(!servo2.move.is_reached){
      // Проверяем окончание движения в зависимости от направления
      if(servo2.pwm.vCCR_step > 0){
	    if(servo2.pwm.vCCR > servo2.pwm.target_vCCR){
		servo2.move.is_reached = 1;
		goto end_servo2;
	    }
      }else if(servo2.pwm.vCCR_step < 0){
	    if(servo2.pwm.vCCR < servo2.pwm.target_vCCR){
		servo2.move.is_reached = 1;
		goto end_servo2;
	    }
      }

      *(servo2.pwm.pulse) = (int32_t)servo2.pwm.pulse_0 + (servo2.pwm.vCCR >> 16);
      servo2.pwm.vCCR += servo2.pwm.vCCR_step;
end_servo2:{}
  }

}


bool Servo1_is_reached(){
  return servo1.move.is_reached;
}

bool Servo2_is_reached(){
  return servo2.move.is_reached;
}

